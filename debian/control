Source: cmake
Section: devel
Priority: optional
Maintainer: Raul Tambre <raul.tambre@clevon.com>
Homepage: https://cmake.org/
Vcs-Browser: https://gitlab.com/clevon/debian/cmake
Vcs-Git: https://gitlab.com/clevon/debian/cmake.git
Rules-Requires-Root: no
Standards-Version: 4.6.2
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 libarchive-dev,
 libcurl4-openssl-dev,
 libexpat1-dev,
 libjsoncpp-dev,
 libncurses5-dev,
 librhash-dev,
 libssl-dev,
 libuv1-dev,
 procps,
 qtbase5-dev,
 zlib1g-dev,
Build-Depends-Indep:
 dh-sequence-sphinxdoc <!nodoc>,
 python3-sphinx <!nodoc>,
 python3-sphinxcontrib.qthelp <!nodoc>,

Package: cmake
Architecture: any
Multi-Arch: foreign
Depends:
 cmake-data (= ${source:Version}),
 procps,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: build-essential, ninja-build
Suggests: cmake-doc
Description: Cross-platform, open-source build system generator
 CMake is used to control the software compilation process using
 simple platform and compiler independent configuration files. CMake
 generates native makefiles and workspaces that can be used in the
 compiler environment of your choice. CMake is quite sophisticated: it
 is possible to support complex environments requiring system
 configuration, pre-processor generation, code generation, and template
 instantiation.
 .
 CMake was developed by Kitware as part of the NLM Insight
 Segmentation and Registration Toolkit project. The ASCI VIEWS project
 also provided support in the context of their parallel computation
 environment. Other sponsors include the Insight, VTK, and VXL open
 source software communities.

Package: cmake-curses-gui
Architecture: any
Depends: cmake (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: curses-based user interface for CMake (ccmake)
 CMake is used to control the software compilation process using simple
 platform and compiler independent configuration files. CMake generates native
 makefiles and workspaces that can be used in the compiler environment of your
 choice.
 .
 This package provides the CMake curses interface. Project configuration
 settings may be specified interactively through this GUI. Brief instructions
 are provided at the bottom of the terminal when the program is running. The
 main executable file for this GUI is "ccmake".

Package: cmake-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: CMake data files (modules, templates and documentation)
 This package provides CMake architecture independent data files (modules,
 templates, documentation etc.). Unless you have cmake installed, you probably
 do not need this package.

Package: cmake-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Build-Profiles: <!nodoc>
Depends:
 cmake-data (= ${source:Version}),
 ${misc:Depends},
 ${sphinxdoc:Depends},
Suggests: cmake
Built-Using: ${sphinxdoc:Built-Using}
Description: Full HTML CMake documentation
 CMake is used to control the software compilation process using simple
 platform and compiler independent configuration files. CMake generates native
 makefiles and workspaces that can be used in the compiler environment of your
 choice.
 .
 This package provides additional documentation in various formats like HTML or
 plain text.

Package: cmake-qt-gui
Architecture: any
Build-Profiles: <!stage1>
Depends: cmake (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Provides: cmake-gui
Description: Qt based user interface for CMake (cmake-gui)
 CMake is used to control the software compilation process using simple
 platform and compiler independent configuration files. CMake generates native
 makefiles and workspaces that can be used in the compiler environment of your
 choice.
 .
 This package provides the CMake Qt based GUI. Project configuration
 settings may be specified interactively. Brief instructions are provided at
 the bottom of the window when the program is running. The main executable
 file for this GUI is "cmake-gui".
